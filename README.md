# AvenueCode

## Instructions for running tests:

Inside the project folder, please type the following command:

mvn test

## Instructions for running the application:

Inside the project folder, please type the following command

mvn spring-boot:run

### Products

1. Adding products to the database:

   a. Please, use the following url to add a product via REST (JAX-RS)

	http://localhost:8080/product/addProduct via POST

   The json entity must be in this format:

	{"name":"Notebook","description":"Awesome game notebook!","images":null}

   If you wish to add a product with a parent product, use this **(please notice that the parent product must exist and have a valid id)**

	{"name":"HeadPhone","description":"Very high quality","images":null,"parentProduct":{"id":1}}}

2. Getting products (excluding relationships):

   a. 
   
	http://localhost:8080/product/getall via GET

3. Getting products (including relationships)

   a. 
   
	http://localhost:8080/product/getallwithchildren via GET

4. Getting specific product (excluding relationships)

	http://localhost:8080/product/{id} via GET for example:
	http://localhost:8080/product/1

5. Getting specific product (including all relationships)

	http://localhost:8080/product/1/getallwithchildren via GET

6. Getting specific product (with images):

	http://localhost:8080/product/1/getwithimages via GET

7. Getting specific product (with parent product):

	http://localhost:8080/product/1/getwithparent via GET
	
8. Updating product
	
   a. Please use the following url to update a product:
	
	http://localhost:8080/product/updateProduct via POST

   b. the json entity must be in this format:
	
	{"id":1,"name":"ULTRA NOTEBOOK SLIM","description":"woooot"}

9. Deleting product (please remove child products and images before removing parent product)

	http://localhost:8080/product/deleteProduct/2 via DELETE

### Images

1. Adding images to the database:

   a. Please, use the following url to add an image via REST (JAX-RS)

	http://localhost:8080/image/addImage via POST

   The json entity must be in this format:

	{"type":"png","product":{"id":1}}
	
2. Getting images:

	http://localhost:8080/image/getall via GET
	
2. Getting specific image:

	http://localhost:8080/image/1 via GET

3. Updating image:

	http://localhost:8080/image/updateImage via POST
	
   The json entity must be in this format:

	{"id":1,"type":".magic","product":{"id":1}}
	
4. Deleting image:

	http://localhost:8080/image/deleteImage/1 via DELETE
	
  
	
	

