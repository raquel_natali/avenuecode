package com.avenuecode.ProvaAvenueCode.util;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.avenuecode.ProvaAvenueCode.endpoints.ImageEndPoint;
import com.avenuecode.ProvaAvenueCode.endpoints.ProductEndpoint;

@Component
public class JerseyConfig extends ResourceConfig{
    public JerseyConfig() {
        registerEndpoints();
    }

    private void registerEndpoints() {
    	register(ProductEndpoint.class);
    	register(ImageEndPoint.class);
    }
}
