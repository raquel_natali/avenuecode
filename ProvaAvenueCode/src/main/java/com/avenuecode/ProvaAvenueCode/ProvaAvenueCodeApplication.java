package com.avenuecode.ProvaAvenueCode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvaAvenueCodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvaAvenueCodeApplication.class, args);
	}
}
