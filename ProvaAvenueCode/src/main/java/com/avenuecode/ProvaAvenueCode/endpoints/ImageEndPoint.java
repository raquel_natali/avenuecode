package com.avenuecode.ProvaAvenueCode.endpoints;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.avenuecode.ProvaAvenueCode.entities.Image;
import com.avenuecode.ProvaAvenueCode.entities.Product;
import com.avenuecode.ProvaAvenueCode.repository.ImageRepository;
import com.avenuecode.ProvaAvenueCode.repository.ProductRepository;

@Component
@Path("/image")
@Produces("application/json")
@Consumes("application/json")
public class ImageEndPoint {
	@Autowired
	ImageRepository imageRepository;
	@Autowired
	ProductRepository productRepository;
	@GET
	@Path("/getall")
	public List<Image> getAll(){
		return imageRepository.findAll();
	}

	@GET
	@Path("/{id}")
	public Image getById(@PathParam("id")Long id) {
		return imageRepository.findOne(id);
	}

	@POST
	@Path("/addImage")
	public void addImage(Image Image) {
		Product product = productRepository.findOne(Image.getProduct().getId());
		product.setImages(new ArrayList<>());
		List<Image> images = imageRepository.findAll();
		for(int x = 0; x < images.size(); x++) {
			Image image = images.get(x);
			if(image.getProduct().getId().equals(product.getId())) {
				product.getImages().add(image);
			}
		}
		Image.setProduct(product);
		imageRepository.save(Image);	
		images = product.getImages();
		if(images == null)
			product.setImages(new ArrayList<>());
		product.getImages().add(Image);
	}

	@POST
	@Path("/updateImage")
	public void updateImage(Image Image) {
		Image oldImage = imageRepository.findOne(Image.getId());
		if(oldImage != null) {
			oldImage.setType(Image.getType());
			imageRepository.save(oldImage);
		}
		else {
			addImage(Image);
		}
	}
	@DELETE
	@Path("/deleteImage/{id}")
	public void deleteImage(@PathParam("id")Long id) {
		imageRepository.delete(id);
	}
}
