package com.avenuecode.ProvaAvenueCode.endpoints;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.avenuecode.ProvaAvenueCode.entities.Image;
import com.avenuecode.ProvaAvenueCode.entities.Product;
import com.avenuecode.ProvaAvenueCode.repository.ImageRepository;
import com.avenuecode.ProvaAvenueCode.repository.ProductRepository;

@Component
@Path("/product")
@Produces("application/json")
@Consumes("application/json")
public class ProductEndpoint {
	@Autowired
	ProductRepository productRepository;

	@Autowired
	ImageRepository imageRepository;
	
	@GET
	@Path("/getall")
	public List<Product> getAll(){
		List<Product> products = productRepository.findAll();
		for(int i = 0; i < products.size();i++)
		{
			Product product = products.get(i);
			product.setImages(null);
			product.setParentProduct(null);
		}
		return products;
	}

	@GET
	@Path("/getallwithchildren")
	public List<Product> getAllWithChildren(){
		List<Product> products = productRepository.findAll();
		for(int i = 0; i < products.size();i++)
		{
			Product product = products.get(i);
			product.setImages(new ArrayList<>());
			List<Image> images = imageRepository.findAll();
			for(int x = 0; x < images.size(); x++) {
				Image image = images.get(x);
				if(image.getProduct().getId().equals(product.getId())) {
					product.getImages().add(image);
				}
			}
			
		}
		return products;
	}
	@GET
	@Path("/{id}")
	public Product getById(@PathParam("id")Long id) {
		Product product = productRepository.findOne(id);
		product.setParentProduct(null);
		product.setImages(null);
		return product;
	}

	@GET
	@Path("/{id}/getallchildren")
	public Product getByIdWithChildren(@PathParam("id") Long id) {
		Product product = productRepository.findOne(id);
		return product;
	}
	
	@GET
	@Path("/{id}/getwithparent")
	public Product getByIdWithParent(@PathParam("id") Long id) {
		Product product = productRepository.findOne(id);
		product.setImages(null);
		Product parentProduct = product.getParentProduct();
//		parentProduct.setImages(null);
		product.setParentProduct(parentProduct);
		return product;
	}
	
	@GET
	@Path("/{id}/getwithimages")
	public Product getByIdWithImages(@PathParam("id") Long id) {
		Product product = productRepository.findOne(id);
		product.setParentProduct(null);
		return product;
	}
	
	@POST
	@Path("/addProduct")
	public void addProduct(Product product) {
		if(product.getParentProduct() != null) {
			Product parentProduct = getById(product.getParentProduct().getId());
			product.setParentProduct(parentProduct);
		}
		productRepository.save(product);
	}

	@POST
	@Path("/updateProduct")
	public void updateProduct(Product product) {
		Product oldProduct = productRepository.findOne(product.getId());
		if(oldProduct != null) {
			oldProduct.setDescription(product.getDescription());
			oldProduct.setName(product.getName());
			if(oldProduct.getParentProduct() != null) {
				Product parentProduct = getById(product.getParentProduct().getId());
				oldProduct.setParentProduct(parentProduct);
			}
			productRepository.save(oldProduct);
		}
		else {
			addProduct(product);
		}
	}
	@DELETE
	@Path("/deleteProduct/{id}")
	public void deleteProduct(@PathParam("id")Long id) {
		productRepository.delete(id);
	}
}
