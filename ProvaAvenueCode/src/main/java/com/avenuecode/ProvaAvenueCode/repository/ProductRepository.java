package com.avenuecode.ProvaAvenueCode.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avenuecode.ProvaAvenueCode.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

}
