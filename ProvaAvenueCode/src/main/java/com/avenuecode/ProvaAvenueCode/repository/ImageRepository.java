package com.avenuecode.ProvaAvenueCode.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avenuecode.ProvaAvenueCode.entities.Image;

public interface ImageRepository extends JpaRepository<Image, Long>{

}
