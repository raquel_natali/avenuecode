package com.avenuecode.ProvaAvenueCode;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.ProvaAvenueCode.entities.Image;
import com.avenuecode.ProvaAvenueCode.entities.Product;
import com.avenuecode.ProvaAvenueCode.repository.ImageRepository;
import com.avenuecode.ProvaAvenueCode.repository.ProductRepository;
import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProvaAvenueCodeApplicationTests {

	@Autowired
	ImageRepository imageRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	public Product insercao() {
		Product product = new Product();
		product.setId(98L);
		product.setDescription("Beautiful and powerful notebook");
		product.setName("SUPER UBER NOTEBOOK");
		productRepository.save(product);
		return product;
	}
	
	public Image insercaoImagem() {
		Image image = new Image();
		image.setId(1L);
		image.setType("png");
		return image;
	}
	
	@Test
	public void contextLoads() {
		Product product = insercao();
		Image image = insercaoImagem();
		image.setProduct(product);
		product.setImages(new ArrayList<>());
		product.getImages().add(image);
		assertThat(product.getName()).isEqualTo("SUPER UBER NOTEBOOK");
		assertThat(product.getImages().size()).isGreaterThan(0);
	}

}
